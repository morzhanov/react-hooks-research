import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import State from "./features/State";
import Effect from "./features/Effect";
import Context from "./features/Context";
import Reducer from "./features/Reducer";
import Callback from "./features/Callback";
import Memo from "./features/Memo";
import Ref from "./features/Ref";
import ImperativeMethods from "./features/ImperativeMethods";
import Custom from "./features/Custom";
import "./App.css";

const Header = () => (
  <header>
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/state">useState</Link>
        </li>
        <li>
          <Link to="/effect">useEffect</Link>
        </li>
        <li>
          <Link to="/context">useContext</Link>
        </li>
        <li>
          <Link to="/reducer">useReducer</Link>
        </li>
        <li>
          <Link to="/callback">useCallback</Link>
        </li>
        <li>
          <Link to="/memo">useMemo</Link>
        </li>
        <li>
          <Link to="/ref">useRef</Link>
        </li>
        <li>
          <Link to="/imperative">useImperativeMethods</Link>
        </li>
        <li>
          <Link to="/custom">customHook</Link>
        </li>
      </ul>
    </nav>
  </header>
);

const Home = () => <main>Home page</main>;

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/state" component={State} />
            <Route path="/effect" component={Effect} />
            <Route path="/context" component={Context} />
            <Route path="/reducer" component={Reducer} />
            <Route path="/callback" component={Callback} />
            <Route path="/memo" component={Memo} />
            <Route path="/ref" component={Ref} />
            <Route path="/imperative" component={ImperativeMethods} />
            <Route path="/custom" component={Custom} />
          </Switch>
        </>
      </BrowserRouter>
    );
  }
}

export default App;

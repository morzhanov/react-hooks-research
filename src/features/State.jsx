import React, { useState } from "react";

const State = () => {
  const [count, setCount] = useState(0);

  return (
    <main>
      Count: {count}
      <button onClick={() => setCount(0)}>Reset</button>
      <button onClick={() => setCount(prevCount => prevCount + 1)}>+</button>
      <button onClick={() => setCount(prevCount => prevCount - 1)}>-</button>
    </main>
  );
};

export default State;

import React, { useState } from "react";

const useCounter = initialCount => {
  const [count, setCount] = useState(initialCount);
  return {
    value: count,
    increase: () => setCount(prevCount => prevCount + 1),
    decrease: () => setCount(prevCount => prevCount - 1),
    reset: () => setCount(initialCount)
  };
};

const Counter = ({ initialCount = 0 }) => {
  const counter = useCounter(initialCount);
  return (
    <main>
      Count: {counter.value}
      <br />
      <br />
      <button onClick={counter.reset}>Reset</button>
      <button onClick={counter.increase}>+</button>
      <button onClick={counter.decrease}>-</button>
    </main>
  );
};

export default Counter;

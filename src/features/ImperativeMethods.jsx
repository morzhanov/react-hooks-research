//useImperativeMethods customizes the instance value that is exposed
// to parent components when using ref. As always, imperative code
// using refs should be avoided in most cases. useImperativeMethods should be used with forwardRef
import React, { useImperativeMethods, useRef, forwardRef } from "react";

let FancyInput = (props, ref) => {
  const inputRef = useRef();
  useImperativeMethods(ref, () => ({
    focus: () => {
      inputRef.current.focus();
    }
  }));
  return (
    <main>
      <input ref={inputRef} />
    </main>
  );
};

FancyInput = forwardRef(FancyInput);

export default FancyInput;

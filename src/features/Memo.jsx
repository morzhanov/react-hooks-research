import React, { useMemo } from "react";

const Table = ({ data }) => (
  <div>
    {data.map(el => (
      <div>{el}</div>
    ))}
  </div>
);

const ExpensiveComputation = ({ data = [0, 1, 2, 3, 4] }) => {
  const transformedData = useMemo(
    () => {
      return data.sort();
    },
    [data]
  );
  return (
    <main>
      <Table data={transformedData} />
    </main>
  );
};

export default ExpensiveComputation;

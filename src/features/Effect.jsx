import React, { useState, useEffect } from "react";

const Effect = () => {
  const [size, setSize] = useState(window);

  const handleSizeChange = ({ target }) => setSize(target);

  useEffect(() => {
    window.addEventListener("resize", handleSizeChange);

    // Specify how to clean up after this effect:
    return () => window.removeEventListener("resize", handleSizeChange);
  });

  return (
    <main>
      Window size is: width: {size.innerWidth}, height: {size.innerHeight}
    </main>
  );
};

export default Effect;

/// useMutationEffect
/// The signature is identical to useEffect, but it fires synchronously during the same 
/// phase that React performs its DOM mutations, before sibling components have been updated. Use this to perform custom DOM mutations.

/// Prefer the standard useEffect when possible to avoid blocking visual updates.

/// useLayoutEffect
/// The signature is identical to useEffect, but it fires synchronously after all DOM mutations. Use this to read layout from the DOM and synchronously re-render. Updates scheduled inside useLayoutEffect will be flushed synchronously, before the browser has a chance to paint.

/// Prefer the standard useEffect when possible to avoid blocking visual updates.

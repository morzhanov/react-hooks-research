import React, { useContext } from "react";

const MyContext = React.createContext({ value: "42" });

const Context = () => {
  const myContext = useContext(MyContext);

  return (
    <main>
      Value from my context is: <strong>{myContext.value}</strong>
    </main>
  );
};

export default Context;

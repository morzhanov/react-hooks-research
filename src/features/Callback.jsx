import React, { useCallback } from "react";

const doSomething = id => console.log(id);

const ExpensiveComponent = props => <main {...props}>Expensive Component</main>;

const FrequentlyRerenders = ({ id }) => {
  return (
    <ExpensiveComponent onEvent={useCallback(() => doSomething(id), [id])} />
  );
};

export default FrequentlyRerenders;
